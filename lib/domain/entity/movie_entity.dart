
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class MovieEntityReq extends EntityReq {
  final int page;

  MovieEntityReq({
    @required this.page,
  }) : super([page]);
}

abstract class EntityReq extends Equatable {
  final List props;

  EntityReq([this.props = const []]) : super([props]);
}