import 'package:dartz/dartz.dart';
import 'package:movietest/core/functions.dart';
import 'package:movietest/core/generic_failures.dart';
import 'package:movietest/data/models/movie_request.dart';
import 'package:movietest/data/models/movie_response.dart';
import 'package:movietest/data/repository/movie_repository.dart';
import 'package:movietest/domain/entity/movie_entity.dart';

class MovieLogic {
  MovieRepository movierepository;

  MovieLogic() {
    movierepository = MovieRepository();
  }


  Future<Either<GenericFailure, MovieResponse>> getMovie(MovieEntityReq req) async{
    var reqM = MovieModelReq(
      page: req.page
    );
    Either<GenericFailure, MovieResponse> resM = await movierepository.getMovies(reqM);

    if (resM.isLeft()) {
      return Left(getLeftOfEither(resM));
    } else {
      var resMright = (getRightOfEither(resM) as MovieResponse);

      //var res = MovieResponse(resMright.uuid);

      return Right(resMright);
    }
  }
}
