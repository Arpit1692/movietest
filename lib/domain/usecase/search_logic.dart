import 'package:dartz/dartz.dart';
import 'package:movietest/core/functions.dart';
import 'package:movietest/core/generic_failures.dart';
import 'package:movietest/data/models/search_request.dart';
import 'package:movietest/data/models/search_response.dart';
import 'package:movietest/data/repository/search_repository.dart';
import 'package:movietest/domain/entity/search_entity.dart';

class SearchLogic {
  SearchRepository searchrepository;

  SearchLogic() {
    searchrepository = SearchRepository();
  }


  Future<Either<GenericFailure, SearchResponse>> searchMovie(SearchEntityReq req) async{
    var reqM = SearchModelReq(
        query: req.query
    );
    Either<GenericFailure, SearchResponse> resM = await searchrepository.searchMovies(reqM);

    if (resM.isLeft()) {
      return Left(getLeftOfEither(resM));
    } else {
      var resMright = (getRightOfEither(resM) as SearchResponse);
      return Right(resMright);
    }
  }
}
