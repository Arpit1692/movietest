import 'package:flutter/material.dart';
import 'package:movietest/presentation/ui/detail_page.dart';
import 'package:movietest/presentation/ui/homepage.dart';
import 'package:movietest/presentation/ui/search_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: MyHomePage.id,
      routes: {
        MyHomePage.id: (context) => MyHomePage(),
        DetailsPage.id: (context) => DetailsPage(),
        SearchPage.id: (context) => SearchPage(),
      },
    );
  }
}


