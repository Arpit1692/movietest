import 'package:dartz/dartz.dart';

getLeftOfEither(Either<Object, Object> either) {
  var left = either.fold(
    (left) => left,
    (right) => null,
  );
  return left;
}

getRightOfEither(Either<Object, Object> either) {
  var right = either.fold(
    (left) => null,
    (right) => right,
  );
  return right;
}
