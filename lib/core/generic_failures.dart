import 'package:equatable/equatable.dart';

abstract class GenericFailure extends Equatable {
  GenericFailure([List properties = const <dynamic>[]]) : super(properties);
}


