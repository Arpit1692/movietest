
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class SearchModelReq extends ModelReq {
  final String query;

  SearchModelReq({
    @required this.query,
  }) : super([query]);
}

abstract class ModelReq extends Equatable {
  final List props;

  ModelReq([this.props = const []]) : super([props]);
}