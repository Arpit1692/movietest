
class SearchResponse {
  final List result;
  final int total_pages;
  final int page;
  final int total_result;

  SearchResponse({this.total_pages,this.page,this.total_result,this.result});

  factory SearchResponse.fromJson(Map<String, dynamic> json) {
    return SearchResponse(
      result: json['results'],
      total_pages: json['total_pages'],
      page: json['page'],
      total_result: json['total_results'],
    );
  }
}