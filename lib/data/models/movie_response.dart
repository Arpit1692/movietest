

class MovieResponse {
  final List result;
  final int total_pages;
  final int page;
  final int total_result;

  MovieResponse({this.total_pages,this.page,this.total_result,this.result});

  factory MovieResponse.fromJson(Map<String, dynamic> json) {
    return MovieResponse(
      result: json['results'],
      total_pages: json['total_pages'],
      page: json['page'],
      total_result: json['total_results'],
    );
  }
}