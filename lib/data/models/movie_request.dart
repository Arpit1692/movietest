
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class MovieModelReq extends ModelReq {
  final int page;

  MovieModelReq({
    @required this.page,
  }) : super([page]);
}

abstract class ModelReq extends Equatable {
  final List props;

  ModelReq([this.props = const []]) : super([props]);
}