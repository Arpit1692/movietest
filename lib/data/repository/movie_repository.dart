import 'package:dartz/dartz.dart';
import 'package:movietest/core/generic_failures.dart';
import 'package:movietest/data/models/movie_request.dart';
import 'package:movietest/data/models/movie_response.dart';
import 'package:movietest/data/source/movie_datasource.dart';

class MovieRepository {
  MovieDataSource movieDataSource;

  MovieRepository() {
    movieDataSource = MovieSource();
  }

  Future<Either<GenericFailure, MovieResponse>> getMovies(MovieModelReq req) async {
    return await movieDataSource.getMovies(req);
  }
}
