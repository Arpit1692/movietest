import 'package:dartz/dartz.dart';
import 'package:movietest/core/generic_failures.dart';
import 'package:movietest/data/models/movie_request.dart';
import 'package:movietest/data/models/movie_response.dart';
import 'package:movietest/data/models/search_request.dart';
import 'package:movietest/data/models/search_response.dart';
import 'package:movietest/data/source/movie_datasource.dart';
import 'package:movietest/data/source/search_data_source.dart';

class SearchRepository {
  SearchDataSource searchDataSource;

  SearchRepository() {
    searchDataSource = SearchSource();
  }

  Future<Either<GenericFailure, SearchResponse>> searchMovies(SearchModelReq req) async {
    return await searchDataSource.searchMovies(req);
  }
}
