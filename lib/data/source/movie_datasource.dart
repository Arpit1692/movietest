import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:movietest/core/generic_failures.dart';
import 'package:movietest/data/models/movie_request.dart';
import 'package:movietest/data/models/movie_response.dart';
import 'package:http/http.dart' as http;

abstract class MovieDataSource {
  Future<Either<GenericFailure, MovieResponse>> getMovies(MovieModelReq req);
}

class MovieSource implements MovieDataSource {
  MovieSource();

  @override
  Future<Either<GenericFailure, MovieResponse>> getMovies(MovieModelReq req) async {
    final response = await http.get('https://api.themoviedb.org/3/movie/now_playing?api_key=bbb379137132355d2b32b373571084d9&language=en-US&page=${req.page}');

    if (response.statusCode == 200) {
      return Right(
       MovieResponse.fromJson(json.decode(response.body))
      );
    } else {
      throw Exception('Failed to load movies');
    }
  }
}
