
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class DetailsPage extends StatelessWidget{
  static const String id = 'detail_page';
  final data;

  DetailsPage({Key key, this.data}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
       centerTitle: true,
       title: Text(data['title'],style: TextStyle(color: Colors.black),),
      ),
      body: SingleChildScrollView(
       child: Column(
         children: <Widget>[
           Stack(
             fit: StackFit.passthrough,
             children: <Widget>[
               Image.network('http://image.tmdb.org/t/p/w185${data['backdrop_path']}',width:double.infinity,fit: BoxFit.fitWidth,),
               Padding(
                 padding: EdgeInsets.all(20),
                 child: Row(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   children: <Widget>[
                     Image.network('http://image.tmdb.org/t/p/w185${data['poster_path']}',width: 150,),
                     Padding(
                       padding: EdgeInsets.only(left:10.0,top: 70.0,right:30.0,bottom:0.0),
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: <Widget>[
                           Text("Rating ",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 24),),
                           SizedBox(height: 10,),
                           RatingBar(
                             initialRating: data['vote_count']/200,
                             minRating: 0,
                             direction: Axis.horizontal,
                             allowHalfRating: true,
                             itemCount: 5,
                             itemSize: 20,
                             itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                             itemBuilder: (context, _) => Icon(Icons.star, color: Colors.amber,size: 10,),

                           ),
                           SizedBox(height: 10,),
                           Text('Released: ${data['release_date']}',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                           SizedBox(height: 20,),
                         ],
                       ),
                     )
                   ],
                 ),
               )
             ],
           ),
           Padding(
             padding: EdgeInsets.all(20),
             child: Column(
               mainAxisAlignment: MainAxisAlignment.start,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     Text(data['original_title'],textAlign: TextAlign.end,style: TextStyle(color: Colors.white,fontSize: 24.0),),
                     Genre(data['adult'])
                   ],
                 ),
                 SizedBox(height: 10,),
                 Text(data['overview'],style: TextStyle(),textAlign: TextAlign.justify,),
                 Padding(
                   padding: EdgeInsets.all(20),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     children: <Widget>[
                       Column(
                         children: <Widget>[
                           Text("Popularity",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),
                           SizedBox(height: 10,),
                           Text(data['popularity'].toString(),style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),
                         ],
                       ),
                       Column(
                         children: <Widget>[
                           Text("Votes",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold)),
                           SizedBox(height: 10,),
                           Text(data['vote_count'].toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18)),
                         ],
                       ),
                       Column(
                         children: <Widget>[
                           Text("Language",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),
                           SizedBox(height: 10,),
                           Language(data['original_language'])
                         ],
                       )
                     ],
                   ),
                 ),
               ],
             ),
           ),
         ],
       ),
      )

    );
  }



}

class Language extends StatelessWidget{
  String data;

  Language(this.data):super();

  Widget build(BuildContext context) {
    return Text((() {
      if(data == "ja"){
        return "Japanese";
      }
      return "English";
    })(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18));
  }
}

class Genre extends StatelessWidget {
  bool data;

  Genre(this.data):super();

  Widget build(BuildContext context) {
    return Text((() {
      if(data){
        return "A";
      }
      return "U/A";
    })(),style: TextStyle(color: Colors.white,fontSize: 18),);
  }
}