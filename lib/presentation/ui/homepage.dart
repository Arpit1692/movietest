
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:movietest/presentation/bloc/movie_bloc.dart';
import 'package:movietest/presentation/ui/detail_page.dart';
import 'package:movietest/presentation/ui/search_page.dart';

class MyHomePage extends StatefulWidget {
  static const String id = 'home_page';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<MyHomePage> with TickerProviderStateMixin{
  int count = 1;
  bool isLoading = false;
  final MovieBloc movieBloc = MovieBloc();
  List<dynamic> res;

  @override
  void initState() {
    super.initState();
    dispatchMovies();
  }
  
  

  dispatchMovies(){
   movieBloc.add(MovieRequest(page: count));
  }

  Future _loadData() async {
    await new Future.delayed(new Duration(seconds: 2)).then((value) =>
        movieBloc.add(MovieRequest(page: count++)));

    setState(() {
      res.clear();   //This List is Growable, I will check for Infinite loop, res.addAll() is not working here currently
                     //So cleared it out for Now.
      isLoading = false;
    });

  }





@override
  void dispose() {
   super.dispose();
  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
    backgroundColor: Colors.white,
    appBar: AppBar(
     centerTitle: true,
     backgroundColor: Colors.white,
     title: Text("The Internet Movie Database",style: TextStyle(color:Colors.black),),
    ),
    floatingActionButton: FloatingActionButton(
      splashColor: Colors.white,
      child: Icon(Icons.search),
      onPressed: (){
       Navigator.of(context).pushNamed(SearchPage.id);
      },
    ),
    body: SafeArea(
    child: Container(
     padding: EdgeInsets.all(10),
     child: Center(
      child: blocListener(context),
   )
    )));
  }

  Widget blocListener(BuildContext context) {
    return BlocListener(
      bloc: movieBloc,
      listener: (BuildContext context, MovieState state) {
        if (state is Error) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Failed",
                      maxLines: 3,
                    ),
                    Icon(Icons.error)
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: blocProvider(context),
    );
  }

  BlocProvider<MovieBloc> blocProvider(BuildContext context) {
    return BlocProvider(
      create: (context) => movieBloc,
      child: BlocBuilder<MovieBloc, MovieState>(
        builder: (context, state) {
          Widget display;
          if (state is Initial) {
            display =  Center(
             child:  SpinKitRipple(
               color: Colors.blue,
               size: 50.0,
               controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
             ),
            );
          }else if(state is Response){
            res = state.res.result;
            display = _buildPaginatedListView(res);
            /*display = Column(
             children: <Widget>[
               Container(
                 height: MediaQuery.of(context).size.height,
                 child: MovieAdapter(state.res.result),
               ),
               Container(
                 height: isLoading ? 50.0 : 0,
                 color: Colors.transparent,
                 child: Center(
                   child: new CircularProgressIndicator(),
                 ),
               ),
             ],
            );*/
          }
          return display;
        },
      ),
    );
  }

  Widget _buildPaginatedListView(List result) {
    return Column(
      children: <Widget>[
        Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading && scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {// start loading data
                  setState(() {
                    isLoading = true;
                  });
                  _loadData();
                }
                return isLoading;
              },
              child: MovieAdapter(result),
            )
        ),
        Container(
          height: isLoading ? 50.0 : 0,
          color: Colors.white70,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
      ],
    );
  }


}

MovieAdapter(List result) {
  return ListView.builder(
   itemCount: result.length,
   itemBuilder: (BuildContext context, int index) {
    return InkWell(
      onTap: (){
        Navigator.push(context,
            MaterialPageRoute(
              builder: (context) => DetailsPage(data: result[index]),
            ));
      },
      child: Container(
        height: 110, width: double.infinity,
        padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  Card(
                      margin: EdgeInsets.all(0), elevation: 0,
                      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(8),),
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: Image.network('http://image.tmdb.org/t/p/w185${result[index]['poster_path']}', height: 100, width: 100, fit: BoxFit.cover)
                  ),
                  Container(width: 10),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(flex: 1,child: Text(result[index]['title'],overflow: TextOverflow.ellipsis,maxLines: 2, style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold)),
                                ),Text("Rating: "+result[index]['vote_average'].toString(), style: TextStyle(color: Colors.black)),
                          ],
                        ),
                        Spacer(),
                        Text(result[index]['overview'], maxLines:3,overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w500)),


                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(height: 10),
            Divider(height: 0)
          ],
        ),
      ),
    );
   }
  );
}




